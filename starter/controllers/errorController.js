const AppError = require('./../utils/appError');

module.exports = (err, req, res, next) => {
    err.statusCode = err.statusCode || 500;
    err.status = err.status || 'error'
    let error = {...err};
    if (error.name==='CastError') error = handleCastErrorDB(error);
    if (error.code===11000) error = handleDuplicateFieldsDB(error);
    if (error.name==='JsonWebTokenError') error = handleJWTError();
    if (error.name ==='TokenExpiredError') error = handleJWTExpiredError()
    sendErrorProd(error, res);
};

const handleJWTExpiredError =() => new AppError('Your token has expired. Please login again', 401);

const handleJWTError = () => new AppError('Invalid token. Please log in again', 401);

const handleCastErrorDB = err => {
    const message = `Invalid ${err.path}: ${err.value}.`;
    return new AppError(message, 400)
};

const handleDuplicateFieldsDB = err => {
    const value = err.errmsg.match(/([""'])(\\?.)*?\1/);
    const message = `Duplicate field value: ${value}. Please use another value`;
    return new AppError(message, 400);
}

const sendErrorProd = (err, res) => {
    if(err.isOperational){
        res.status(err.statusCode).json({
            status: err.status,
            message: err.message
        });
    }
    else {
        console.log('Error', err);
    }
};

